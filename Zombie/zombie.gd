extends Area2D

@export var baseMoveSpeed : float = 1.0

@export var hp : float : set = set_hp
@export var curMoveSpeed : float = baseMoveSpeed
@export var state : ZombieStates = ZombieStates.WALK

var player : CharacterBody2D : get = get_player

enum ZombieStates {WALK, STUN, ATTACK, DIE}

func _ready():
	pass

func _process_these_balls(delta):
	match state:
		ZombieStates.WALK:
			position.x -= curMoveSpeed
		ZombieStates.STUN:
			pass
		ZombieStates.ATTACK:
			pass
		ZombieStates.DIE:
			pass
		_:
			print("OOOPSIED I MAKE A FUC*Y WUCKY OwO *GODBESS* t <- CROSS 4 JESUS")

func set_hp(new_value):
	if (new_value <= 0):
		state = ZombieStates.DIE
	
	hp = new_value

func get_player():
	if player == null:
		player = $Player
	
	return player
