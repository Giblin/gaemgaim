extends CharacterBody2D

@export var SPEED = 300.0

func _physics_process(delta):
	#if Input.is_action_just_pressed("ui_accept"):
		#velocity.y = JUMP_VELOCITY
		
	var input_dir = Input.get_vector("Move_Left", "Move_Right", "Move_Up", "Move_Down").normalized()
	if input_dir:
		velocity.x = input_dir.x * SPEED
		velocity.y = input_dir.y * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.y = move_toward(velocity.y, 0, SPEED)

	move_and_slide()
